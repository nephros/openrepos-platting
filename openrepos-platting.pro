TARGET = openrepos-platting
VERSION = "0.9.0"
GIT_VERSION = $$system(git --git-dir $$PWD/.git --work-tree $$PWD describe --always)
VERSION_STRING = "Version " + $${VERSION}

# Qt things
QT += core gui dbus quick qml

# Sailfish App
CONFIG += sailfishapp
CONFIG += link_pkgconfig

PKGCONFIG += sailfishapp
INCLUDEPATH += /usr/include/sailfishapp
QMAKE_RPATHDIR += /usr/share/$${TARGET}/lib

# Ring Client API
CONFIG += c++17
QMAKE_CXXFLAGS += -std=c++17
LIBS += /usr/lib/libringclient.so
INCLUDEPATH += /usr/include/libringclient

SOURCES += src/main.cpp
INCLUDES += src/jami_deps.h

target.path = /usr/bin

desktopfile.files = $${TARGET}.desktop
desktopfile.path = /usr/share/applications
desktopicon.files = $${TARGET}.svg
desktopicon.path = /usr/share/icons/hicolor/scalable/apps/


qml.files = qml
qml.path = /usr/share/$${TARGET}

icons.files = icons
icons.path = /usr/share/icons/hicolor/86x86/apps

INSTALLS += target qml icons desktopfile desktopicon

OTHER_FILES += $$files(rpm/*)
RESOURCES += \
    ressource.qrc

OTHER_FILES += \
    rpm/$$TARGET.yaml \
    rpm/$$TARGET.spec \
    qml/AccountPage.qml
    qml/AccountSettings.qml
    qml/CallView.qml
    qml/HistoryView.qml
    qml/SettingView.qml
    qml/main.qml
    qml/components/ToolBar.qml
    qml/components/TabView.qml
    qml/components/HistoryDelegate.qml
    qml/components/DialPad.qml
    qml/components/CallDelegateItem.qml
    qml/components/CallDelegate.qml

HEADERS +=

