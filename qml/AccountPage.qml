import QtQuick 2.0
import Sailfish.Silica 1.0
import JamiClient.Account 1.0

Page {
    id: accountPage

    ListModel {
        id: dummyModel
        ListElement {
            name: "one"
        }
        ListElement {
            name: "two"
        }
        Component.onCompleted: console.log(JSON.stringify(this, null, 2))
    }
    PageHeader {
        title: "Account"
    }
    SilicaControl {
        anchors.fill: parent
        //color: Theme.highlightBackgroundColor

        //Account delegate
        Component {
            id: accountDelegate
            Item {
                width: parent.parent.width
                height: Theme.itemSizeMedium
                Label {
                    id: usernameLabel
                    text: display
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("ici2 " + model.alias + " " + model.object
                                    + " " + AccountListModel.getAccountById(
                                        model.id))
                        mainPageStack.push(accountSettingsPage)
                        accountSettingsPage.account = AccountListModel.getAccountById(
                                    model.id)
                    }
                }
            }
        }

        ListView {
            id: accountListView

            anchors.top: parent.top
            width: parent.width
            height: parent.height

            delegate: accountDelegate
            model: AccountListModel
        }
    }
}
