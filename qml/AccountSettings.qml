import QtQuick 2.0
//import SFLPhone 1.0
import Sailfish.Silica 1.0
import Sailfish.Accounts 1.0


Page {
    id:accountSetting
    property Account account
    Flickable {
        anchors.fill: parent
        contentWidth: parent.width; contentHeight: content.height
        Column {
        id:content
        width:parent.width

        ListItem {
            id: aliasLabel
            TextField {
            text: qsTr("Alias")
                id: aliasValue
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        Label {
            id:protocolLabel
            text:"Protocol"
            //color:Theme.palette.normal.text
        }

        ComboBox {
            id:protocolValue
            label: "Protocol"
            //property string values: ["SIP", "IAX"]
            menu: ContextMenu {
              MenuItem{ text: "SIP" }
              MenuItem{ text: "IAX" }
            }
        }
        ListItem {
            id:serverLabel
            TextField {
            text:"Server"
                id:serverValue
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        ListItem {
            id:usernameLabel
            TextField {
            text:"Username"
                id:usernameValue
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        ListItem {
            id:passwordLabel
            TextField {
            text:"Password"
                id:passwordValue
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        ListItem {
            id:voicemailLabel
            TextField {
            text:"Voicemail"
                id:voicemailValue
                anchors.verticalCenter: parent.verticalCenter
            }
        }
        ListItem {
            id:contactAccountLabel
            TextSwitch {
            text: qsTr("Default account for contacts")
                id: contactAccountValue
                anchors.verticalCenter: parent.verticalCenter
            }
        }

         ListItem {
             id:expireLabel
             TextField {
             text:"Registration expire:"
                 id:expireValue
                 inputMethodHints : Qt.ImhFormattedNumbersOnly
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "accountRegistrationExpire"; value: expireValue.text }

         ListItem {
             id:localLabel
             TextField {
             text:"Inerface"
                 id:localValue
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "localInterface"; value: localValue.text }

         ListItem {
             id:portLabel
             TextField {
             text:"Port"
                 id:portValue
                 inputMethodHints : Qt.ImhFormattedNumbersOnly
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "localPort"; value: portValue.text }

         ListItem {
             id:proxyLabel
             TextField {
             text:"Proxy"
                 id:proxyValue
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "proxy"; value: proxyValue.text }

         ListItem {
             id:sameAsLocalLabel
             TextSwitch {
             text:"Use same as local:"
                 id: sameAsLocalValue
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "publishedSameAsLocal"; value: sameAsLocalValue.checked }

         ListItem {
             id:publishedAddrLabel
             TextField {
             text:"Published address:"
                 id:publishedAddrValue
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "publishedAddress"; value: publishedAddrValue.text }

         ListItem {
             id:publishedPortLabel
             TextField {
             text:"Published port"
                 id:publishedPortValue
                 inputMethodHints : Qt.ImhFormattedNumbersOnly
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "publishedPort"; value: publishedPortValue.text }

         ListItem {
             id:hasStunLabel
             TextSwitch {
             text:"Has STUN"
                 id: hasStunValue
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "sipStunEnabled"; value: hasStunValue.checked }

         ListItem {
             id:stunLabel
             TextField {
             text:"STUN Server"
                 id:stunValue
                 anchors.verticalCenter: parent.verticalCenter
             }
         }
         Binding { target: account; property: "sipStunServer"; value: stunValue.text }

         Label {
             id:overRtpLabel
             text:"DTMF over RTP"
         }
         ComboBox {
             label: "Standard"
             id:overRtpValue
             //property string values: ["SIP", "RTP"]
             menu: ContextMenu {
               MenuItem{ text: "SIP" }
               MenuItem{ text: "RTP" }
             }
         }
         Binding { target: account; property: "dTMFType"; value: overRtpValue.text }

         ListItem {


             Rectangle {
                 id:codecSettings
                 color:"#dddd22"
                 width:parent.width
                 height: codecSettingsCols.height

                 Component {
                     id:audioCodecDelegate
                     Rectangle {
                         height:20
                         width:codecSettings.width
                         //anchors.fill: parent
                         Row {
                             spacing: 5
                                Text {
                                    text:model.name
                                }
                                Text {
                                    text:model.bitrate
                                }
                                Text {
                                    text:model.samplerate
                                }
                         }
                     }
                 }

                 Component {
                     id:videoCodecDelegate
                     Rectangle {
                         Row {
                             spacing: 5
                                Text {
                                    text:"sdfsdf"
                                }
                                Text {
                                    text:"234"
                                }
                         }
                     }
                 }
                 Column {
                     id:codecSettingsCols
                     Text {
                         text:"Audio codecs:"
                     }
                     Repeater {
                         id:audioCodecModelView
                         delegate:audioCodecDelegate
                         width:parent.width
                     }
                     Text {
                         text:"Video codecs:"
                     }
                     Repeater {
                         id:videoCodecModelView
                         delegate:videoCodecDelegate
                         width:parent.width
                     }
                 }
             }
             //TLS enabled field
             Label {
                 id: enableTlsLabel
                 text:"Enable TLS"
                 TextSwitch {
                     id: enableTlsValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsEnable"; value: enableTlsValue.checked }

             //Global TLS listener
             ListItem {
                 id: globalTlsLestenerLabel
                 TextField {
                 text:"Global TLS listener"
                     id: globalTlsListenerValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsListenerPort"; value: globalTlsListenerValue.text }

             //Authority certificate list
             ListItem {
                 id: authorotyCertListLabel
                 TextField {
                 text:"Authority certificate list"
                     id: authorotyCertListValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsCaListFile"; value: authorotyCertListValue.text }

            //Public End Point Certificate
             ListItem {
                 id: publicEndPointCertLabel
                 TextField {
                 text:"Public End Point Certificate"
                     id: publicEndPointCertValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsCertificateFile"; value: publicEndPointCertValue.text }

            //Private Key
             ListItem {
                 id: privateKeyLabel
                 TextField {
                 text: "Private Key"
                     id: privateKeyValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsPrivateKeyFile"; value: privateKeyValue.text }

            //Private Ket password
             ListItem {
                 id: privateKeyPassLabel
                 TextField {
                 text: "Private Ket password"
                     id: privateKeyPassValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsPassword"; value: privateKeyPassValue.text }

            //TLS Protocol Method
             ListItem {
                 id: tlsProtoMethodLabel
                 TextField {
                 text: "TLS Protocol Method"
                     id: tlsProtoMethodValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsMethod"; value: tlsProtoMethodValue.text }

            //TLS cipher list
             ListItem {
                 id: tlsCipherListLabel
                 TextField {
                     id: tlsCipherListValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsCiphers"; value: tlsCipherListValue.text }

            //Outgoing TLS server
             ListItem {
                 id: outgoingTlsServerLabel
                 TextField {
                 text: "Outgoing TLS server"
                     id: outgoingTlsServerValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsServerName"; value: outgoingTlsServerValue.text }

            //Negotiation Timeout (seconds)
             ListItem {
                 id: negoTimeOutSecLabel
                 TextField {
                 text: "Negotiation Timeout (seconds)"
                     id: negoTimeOutSecValue
                     inputMethodHints : Qt.ImhFormattedNumbersOnly
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsNegotiationTimeoutSec"; value: negoTimeOutSecValue.text }

            //Negotiation Timeout (milliseconds)
             ListItem {
                 id: negoTimeOutMSecLabel
                 TextField {
                 text: "Negotiation Timeout (milliseconds)"
                     id: negoTimeOutMSecValue
                     inputMethodHints : Qt.ImhFormattedNumbersOnly
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsNegotiationTimeoutMsec"; value: negoTimeOutMSecValue.text }

            //Verify incoming certificates
             ListItem {
                 id: verifyIncomingCertLabel
                 TextSwitch {
                 text: "Verify incoming certificates"
                     id: verifyIncomingCertValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsVerifyServer"; value: verifyIncomingCertValue.checked }

            //Verify answer certificates
             ListItem {
                 id: verifyAnswerCertLabel
                 TextSwitch {
                 text: "Verify answer certificates"
                     id: verifyAnswerCertValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsVerifyClient"; value: verifyAnswerCertValue.checked }

            //Require certificates for incoming calls
             ListItem {
                 id: requireCertForIncomingLabel
                 TextSwitch {
                 text: "Require certificates for incoming calls"
                     id: requireCertForIncomingValue
                     anchors.verticalCenter: parent.verticalCenter
                 }
             }
             Binding { target: account; property: "tlsRequireClientCertificate"; value: requireCertForIncomingValue.text }


         }
     }
     }

    onAccountChanged: {
        console.log("Account now is "+account.alias+" "+account.audioCodecModel())

        //Basic
        aliasValue.text                 = account.alias
        protocolValue.text              = account.typeName
        serverValue.text                = account.hostname
        usernameValue.text              = account.username
        //passwordValue.text            = account.
        voicemailValue.text             = account.mailbox
        contactAccountValue.checked     = false

        //Advanced
        expireValue.text                = account.accountRegistrationExpire
        localValue.text                 = account.localInterface
        portValue.text                  = account.localPort
        proxyValue.text                 = account.proxy
        sameAsLocalValue.checked        = account.publishedSameAsLocal
        publishedAddrValue.text         = account.publishedAddress
        publishedPortValue.text         = account.publishedPort
        hasStunValue.checked            = account.sipStunEnabled
        stunValue.text                  = account.sipStunServer
        //overRtpValue.checked          = account.dTMFType

        //Security
        enableTlsValue.checked           = account.tlsEnable
        globalTlsListenerValue.text      = account.tlsListenerPort
        authorotyCertListValue.text      = account.tlsCaListFile
        publicEndPointCertValue.text     = account.tlsCertificateFile
        privateKeyValue.text             = account.tlsPrivateKeyFile
        privateKeyPassValue.text         = account.tlsPassword
        tlsProtoMethodValue.text         = account.tlsMethod
        tlsCipherListValue.text          = account.tlsCiphers
        outgoingTlsServerValue.text      = account.tlsServerName
        negoTimeOutSecValue.text         = account.tlsNegotiationTimeoutSec
        negoTimeOutMSecValue.text        = account.tlsNegotiationTimeoutMsec
        verifyIncomingCertValue.checked  = account.tlsVerifyServer
        verifyAnswerCertValue.checked    = account.tlsVerifyClient
        requireCertForIncomingValue.text = account.tlsRequireClientCertificate

        //Codec
        audioCodecModelView.model = account.audioCodecModel()
        videoCodecModelView.model = account.videoCodecModel()

        accountSetting.title = account.alias
    }
}
