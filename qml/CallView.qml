import QtQuick 2.0
import Sailfish.Silica 1.0
//import SFLPhone 1.0
import JamiClient.ConversationView 1.0 as CallModel
import "components"

Page {
    id: callView
    anchors.fill: parent
    PageHeader {
        title: "Calls"
    }
    property Item currentItem: null
    property string currentCallId
    SilicaListView {
        //anchors.fill:parent.parent
        height: parent.height
        width: parent.width
        model: CallModel
        delegate: CallDelegate {
            id: callDelegate
            property bool selected: false
            onSelectedChanged: {
                console.log("Selection changed")
                if (currentItem != null)
                    currentItem.selected = false

                //currentItem = this
            }
        }
        //    DialPad {
        //        id: dialPad
        //        height: Theme.ItemSizeMedium * 3
        //        width:parent.width
        //        y:parent.height-dialPad.height -10
        //        z:10000
        //    }
    }
}
