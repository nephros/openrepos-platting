import QtQuick 2.0
import Sailfish.Silica 1.0
import JamiClient.ConversationModel 1.0 as HistoryModel

Page {
    id: historyView

    PageHeader {
        title: "History"
    }

    property Item currentItemAAA: null

    SilicaControl {
        height: parent.height
        width: parent.width
        CallDelegate {
            id: callDelegate
        }

        SilicaControl {

            height: parent.height - historySearch.height - 10
            width: parent.width
            //color:"transparent"
            z: 2
            clip: true

            HistoryDelegate {
                id: historyDelegate
            }

            SilicaFlickable {
                anchors.fill: parent
                y: 20
                contentWidth: parent.width
                contentHeight: historyCategories.height

                //boundsBehavior:Flickable.StopAtBounds
                Column {
                    id: historyCategories

                    x: 5
                    y: 15
                    spacing: 10
                    width: parent.width

                    Repeater {
                        model: HistoryModel

                        Item {
                            id: categoryRect

                            width: parent.width
                            height: childrenLayout.height + categoryHeader.height + 20

                            SectionHeader {
                                id: categoryHeader
                                text: display
                            }
                            Column {
                                spacing: 10
                                anchors.top: categoryHeader.bottom
                                id: childrenLayout

                                Repeater {
                                    id: childrenView

                                    model: VisualDataModel {
                                        id: childrenVisualDataModel
                                        model: HistoryModel
                                        Component.onCompleted: {
                                            childrenView.model.rootIndex
                                                    = childrenView.model.modelIndex(
                                                        index)
                                            childrenVisualDataModel.delegate = historyDelegate
                                        }
                                    } //childrenVisualDataModel
                                } //childrenView
                            } //childrenLayout
                        } //categoryRect
                    } //HistoryModel Repeater
                } //Column
            } //Flick
        } //historyView

        TextField {
            id: historySearch
            width: parent.width
            placeholderText: "Search"
            y: parent.height - height - 10
        }
    }
}
