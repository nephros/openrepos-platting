import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    id: callDelegateItem
    anchors.margins: 2
    radius: 5
    color: "transparent"

    property bool selected: false

    Row {
        function getStateImage() {
            var state = model.state
            return "../../ressources/callstate2/hold.png"
        }

        SilicaItem {
            objectName: "ubuntushape_image"
            //radius: "medium"
            Image {
                source: "../../ressources/callstate2/hold.png" //getStateImage()
                fillMode: Image.PreserveAspectCrop
            }
            height: Theme.iconSizeLarge
            width: Theme.iconSizeLarge
        }

        Column {
            anchors.verticalCenter: parent.verticalCenter
            Text {
                text: display
                color: Theme.secondaryColor
                font.bold: true
            }
            Text {
                text: model.number
                color: Theme.secondaryColor
            }
        }
    }

    onSelectedChanged: {
        console.log(selected + " Selecting item" + display)
        if (selected) {
            callDelegateItem.color = "#ff0000"
        } else {
            callDelegateItem.color = "transparent"
        }
    }
}
