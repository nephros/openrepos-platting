import QtQuick 2.0
import Sailfish.Silica 1.0

ButtonLayout {
    id: tabView
    z: 1000
    //width: 328
    height: Theme.itemSizeLarge
    property string currentPage: "Calls"

    Button {
        id: rectangle1
        text: "Calls"
        text.horizontalAlignment: Text.AlignHCenter
        onClicked: {
            console.log("Call tab clicked")
            mainPageStack.push(callView)
            //parent.parent.parent.currentPage = "Calls"
        }
        // TODO:
        //icon:
    }

    Button {
        color: "green"
        text: "History"
        text.horizontalAlignment: Text.AlignHCenter
        onClicked: {
            console.log("History tab clicked")
            mainPageStack.push(historyView)
            //parent.parent.parent.currentPage = "History"
        }
        // TODO:
        //icon:
    }

    Button {
        text: "Contacts"
        text.horizontalAlignment: Text.AlignHCenter
        onClicked: {
            console.log("Contact tab clicked")
            parent.parent.parent.currentPage = "Contact"
        }
        // TODO:
        //icon:
    }

    Rectangle {
        text: "Settings"
        text.horizontalAlignment: Text.AlignRight
        onClicked: {
            console.log("Settings tab clicked")
            mainPageStack.push(accountPage)
            parent.parent.parent.currentPage = "Settings"
        }
        // TODO:
        //icon:
    }
}
