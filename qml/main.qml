import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow {
    id: mainView

    //allowedOrientations: Orientation.Portrait, Orientation.LandscapeInverted
    //cover: Component { CoverPlaceholder{} }
    CallView {
        id: callView

        objectName: "callView"
        height: parent.height - 60
        width: parent.width
    }

    HistoryView {
        id: historyView
        objectName: "historyView"
    }
    Page {
        id: contactView
        objectName: "contactView"
    }
    SettingView {
        id: settingView
        objectName: "settingView"
    }
    AccountPage {
        id: accountPage
        objectName: "accountPage"
    }
    AccountSettings {
        id: accountSettingsPage
        objectName: "accountSettingsPage"
    }

    initialPage: Component {
        Page {
            PageHeader {
                id: head
                title: Qt.application.name
            }
            // TODO: Make a Pulley Menu
            SilicaFlickable {
                id: flick
                anchors.top: head.bottom
                height: parent.height - head.height
                width: parent.width
                contentHeight: buttons.height
                ButtonLayout {
                    id: buttons
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    //spacing: Theme.paddingMedium
                    height: Theme.itemSizeLarge
                    width: parent.width / 2
                    anchors.margins: 5
                    //y:15
                    z: 100000
                    Button {
                        icon.source: "../../ressources/call.png"
                        height: Theme.iconSizeLarge
                        width: Theme.iconSizeLarge
                        text: "Calls"
                        onClicked: {
                            console.log("Call tab clicked")
                            pageStack.push(callView)
                            //parent.parent.parent.currentPage = "Calls"
                        }
                    }
                    Button {
                        icon.source: "../../ressources/history.png"
                        height: Theme.iconSizeLarge
                        width: Theme.iconSizeLarge
                        text: "History"
                        onClicked: {
                            console.log("History tab clicked")
                            pageStack.push(historyView)
                            //parent.parent.parent.currentPage = "History"
                        }
                    }
                    Button {
                        icon.source: "../../ressources/contact.png"
                        height: Theme.iconSizeLarge
                        width: Theme.iconSizeLarge
                        text: "Contacts"
                        onClicked: {
                            console.log("Contact tab clicked")
                            pageStack.push(contactView)
                            //parent.parent.parent.parent.currentPage = "Contact"
                        }
                    }
                    Button {
                        icon.source: "../../ressources/tools.png"
                        height: Theme.iconSizeLarge
                        width: Theme.iconSizeLarge
                        text: "Settings"
                        onClicked: {
                            console.log("Settings tab clicked")
                            pageStack.push(accountPage)
                            //parent.parent.parent.parent.currentPage = "Settings"
                        }
                    }
                }
            }
        }
    }


    /*
    //State
   states: [
      State {
         name: "Calls"
         PropertyChanges {target: callView   ; visible: true  }
         PropertyChanges {target: historyView; visible: false }
         PropertyChanges {target: settingView; visible: false }
      },
      State {
         name: "History"
         PropertyChanges {target: callView   ; visible: false }
         PropertyChanges {target: historyView; visible: true  }
         PropertyChanges {target: settingView; visible: false }
      },
      State {
         name: "Contact"
         PropertyChanges {target: callView   ; visible: false }
         PropertyChanges {target: historyView; visible: false }
         PropertyChanges {target: settingView; visible: false }
      },
      State {
         name: "Settings"
         PropertyChanges {target: callView   ; visible: false }
         PropertyChanges {target: historyView; visible: false }
         PropertyChanges {target: settingView; visible: true  }
      }
   ]
   */
}
