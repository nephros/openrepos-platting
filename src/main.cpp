#include <QtQuick>
#include <QtQml>
#include <QDebug>
#include "sailfishapp.h"
#include "jami_deps.h"

static const auto _APP_NAME        = QStringLiteral("Platting");
static const auto _APP_ORGA_NAME   = QStringLiteral("org.nephros.sailfish");
static const auto _APP_DOMAIN_NAME = QStringLiteral("sailfish.nephros.org");

int main(int argc, char *argv[])
 {
     QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
     QScopedPointer<QQuickView> view(SailfishApp::createView());

     app->setApplicationName(_APP_NAME);
     app->setApplicationVersion(QString(VERSION_STRING));
     app->setOrganizationName(_APP_ORGA_NAME);
     app->setOrganizationDomain(_APP_DOMAIN_NAME);

     qmlRegisterType<lrc::api::ContactModel>      ("JamiClient", 1, 0, "ContactModel");
     qmlRegisterType<lrc::api::ConversationModel> ("JamiClient", 1, 0, "ConversationModel");
     qmlRegisterType<lrc::api::AVModel>           ("JamiClient", 1, 0, "AVModel");
     //qmlRegisterType<NewAccountModel>   ("JamiClient", 1, 0, "NewAccountModel");
     //qmlRegisterType<NewCallModel>      ("JamiClient", 1, 0, "NewCallModel");
     //qmlRegisterType<Account>           ("JamiClient", 1, 0, "Account");

     //qmlRegisterUncreatableType<Call>("JamiClient", 1, 0, "Call", "Calls cannot be instanciated this way, use the static constructor instead");
     //qmlRegisterUncreatableType<UserActionModel>("JamiClient", 1, 0, "UserActionModel", "Use the ones provided by Call objects");

     /*
     engine()->rootContext()->setContextProperty("CallModel",CallModel::instance());
     engine()->rootContext()->setContextProperty("HistoryModel",HistoryModel::instance());
     engine()->rootContext()->setContextProperty("AccountListModel",AccountListModel::instance());
     */

     view->setSource(SailfishApp::pathTo(QString("qml/main.qml")));
     view->show();

     return app->exec();
}
